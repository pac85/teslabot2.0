extern crate futures;
extern crate telegram_bot_fork;
extern crate tokio;
extern crate meval;
#[macro_use]
extern crate structopt;
#[macro_use]
extern crate scan_fmt;
extern crate tokio_timer;
extern crate plotters;

use std::env;

use futures::{Stream, future::lazy};
use telegram_bot_fork::*;

mod update_handler;
mod handlers;

use std::time::{Duration, SystemTime};

fn main() {

    let mut runtime = tokio::runtime::current_thread::Runtime::new().unwrap();
    runtime.block_on(lazy(|| {
        let token = env::var("TELEGRAM_BOT_TOKEN").unwrap();
        let api = Api::new(token).unwrap();

        let mut handlers = update_handler::Handlers::new(api.clone());
        handlers::register_handlers(&mut handlers);

        let sys_time = SystemTime::now();
        let wait_duration = Duration::new(1, 0);

        // Convert stream to the stream with errors in result
        let stream = api.stream().then(|mb_update| {
           let res: Result<Result<Update, Error>, ()> = Ok(mb_update);
           res
        });

        tokio::executor::current_thread::spawn(
            stream.for_each(move |update| {

                match update {
                    Ok(update) => {
                        if SystemTime::now().duration_since(sys_time).unwrap() > wait_duration
                        {
                            handlers.handle_update(&update, &api);
                        }
                        else
                        {
                            println!("flushing for another {:?}", wait_duration - SystemTime::now().duration_since(sys_time).unwrap() );
                        }
                    }
                    Err(_) => {}
                }
                Ok(())
            })
        );
        Ok::<_, ()>(())
    })).unwrap();

    runtime.run().unwrap();
}
