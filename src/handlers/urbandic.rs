extern crate reqwest;
extern crate curl;
extern crate scraper;

use super::super::update_handler::*;
use telegram_bot_fork::*;

pub struct Urbandic
{

}

impl Urbandic
{
    pub fn new()-> Self
    {
        Self{}
    }

}

impl UpdateHandler for Urbandic
{
    fn get_cmd(&self) -> &'static str {"urbandic"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);
                if let Some(peace_name) = super::trim_command(self, &data)
                {
                    let mut reply = types::requests::SendMessage::new(  message.to_source_chat(),
                                                                        format!("https://www.urbandictionary.com/define.php?term={}", peace_name.trim())); 
                    reply.parse_mode(types::ParseMode::Markdown);
                    reply.disable_preview();
                    api.spawn(reply);
                }
            }
        }
    }
}
