extern crate reqwest;
extern crate curl;
extern crate scraper;

use super::super::update_handler::*;
use telegram_bot_fork::*;
use self::scraper::{Html, Selector};
use self::curl::easy::Easy;
use std::cell::RefCell;

pub struct Books
{

}

impl Books
{
    pub fn new()-> Self
    {
        Self{}
    }

    fn scrape(page_code: String) -> String
    {
        let document = Html::parse_document(&page_code);
        let selector = Selector::parse(".c tbody tr td:nth-child(3) [title=\"\"]").unwrap();
        /*match document.select(&selector).next()
        {
            Some(node) => "libgen.is".to_owned() + node.value().attr("href").unwrap_or("couldn't find part"),
            None => return "couldn't find part".to_string(),
        }*/
        document.select(&selector).map(|node| format!(  "|[{}]({})\n",
                                                        {
                                                            let mut name = node.text().collect::<String>().replace("[", "").replace("]", "");
                                                            /*if(name.len() > 40) {
                                                                name.truncate(37);
                                                                name.push_str("...");
                                                            }*/
                                                            name
                                                        },
                                                        "http://libgen.is/".to_owned() + node.value().attr("href").unwrap_or(""))).collect()
    }

    fn get_link(part_name: &str) -> String
    {
        if part_name.len() < 3 
        {
            return "libgen.is requires the query to be at least 3 characters".to_owned();
        }
        /*let page_code = match reqwest::get(format!("http://www.allbooks.com/view.jsp?Searchword={}", part_name).as_str())
        {
            Ok(mut res) => res.text().unwrap_or("internal error".to_string()),
            Err(_) => "internal error".to_string(),
        };*/
        let page_code = RefCell::new("".to_owned());
        {
            let mut easy = Easy::new();
            easy.get(true);
            easy.url(format!("http://libgen.is/search.php?req={}&lg_topic=libgen&open=0&view=simple&res=25&phrase=1&column=def", part_name).as_str()).unwrap();
            easy.http_headers({
                let mut list = curl::easy::List::new();
                list.append("user-agent: reqwest/0.9.11");
                list
            });
            let mut transf = easy.transfer();
            transf.write_function(|data| {
                page_code.borrow_mut().push_str(std::str::from_utf8(data).unwrap_or(""));
                Ok(data.len())
            }).unwrap();
            transf.perform().unwrap();
        }

        //println!("{}", page_code.into_inner()); "a".to_string()
        Self::scrape(page_code.into_inner())
    }
}

impl UpdateHandler for Books
{
    fn get_cmd(&self) -> &'static str {"books"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);
                if let Some(peace_name) = super::trim_command(self, &data)
                {
                    let mut reply = types::requests::SendMessage::new(message.to_source_chat(), Books::get_link(peace_name.trim())); 
                    reply.parse_mode(types::ParseMode::Markdown);
                    reply.disable_preview();
                    api.spawn(reply);
                }
            }
        }
    }
}
