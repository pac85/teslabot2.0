use super::super::update_handler::*;
use telegram_bot_fork::*;

pub struct Immediate
{
    command: String,
    answer: String,
}

impl Immediate
{
    pub fn new(command: &str, answer: &str)-> Self
    {
        Self{command: command.to_owned(), answer: answer.to_owned()}
    }
}

impl UpdateHandler for Immediate
{
    fn get_cmd(&self) -> &str {&self.command}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);
                api.spawn(message.text_reply(
                    self.answer.clone()
                ));
            }
        }
    }
}
