use super::update_handler::*;
use telegram_bot_fork::*;

use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;

mod calc;
mod inductor;
mod datasheet;
mod immediate;
mod books;
mod urbandic;
mod alert;
mod integrate;
mod plot;

fn generate_immediates(handlers: &mut Handlers)
{
    let mut file = File::open("immediates.txt").expect("immediates.txt not found");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("immediates.txt not readable");
    for line in contents.lines()
    {
        let mut splitted = line.split('=');
        let command = splitted.next().expect("immediates.txt as invalid syntax");
        let answer = splitted.next().expect("immediates.txt as invalid syntax");

        handlers.add_handler(Box::new(immediate::Immediate::new(command.trim(), answer.trim())));
    }
}

pub fn register_handlers(handlers: &mut Handlers)
{
    handlers.add_handler(Box::new(calc::Calc::new()));
    handlers.add_handler(Box::new(inductor::Inductor::new()));
    handlers.add_handler(Box::new(datasheet::Datasheet::new()));
    handlers.add_handler(Box::new(books::Books::new()));
    handlers.add_handler(Box::new(urbandic::Urbandic::new()));
    //handlers.add_handler(Box::new(plot::Plot::new()));
    //handlers.add_handler(Box::new(integrate::Integrate::new()));
    handlers.add_handler(Box::new(alert::Alert::new()));
    generate_immediates(handlers);
}

fn trim_command<'a, T> (handler: &T, data: &'a str) -> Option<&'a str>
    where T: UpdateHandler
{
    if(data.len() < handler.get_cmd().len()+1){
        return None;
    }
    Some(&data[handler.get_cmd().len()+1 ..])
}

fn get_text_from_update(update: &Update) -> Option<&str>
{
    match update.kind
    {
        UpdateKind::Message(Message{kind:MessageKind::Text{ref data,..},..},..)  => {
            Some(data)
        }
        _ => {
            None
        }
    }
}

/*enum ArgVal structopt does it better, doesn't even compile
{
    StringArg(Option<&'static str>),
    IntArg(Option<i32>),
    FloatArg(Option<f32>),
}

impl ArgVal
{
    pub fn new(val_str: &'static str, arg_type: &Self) -> Self
    {
        match arg_type
        {
            ArgVal::StringArg(_) => ArgVal::StringArg(Some(val_str)),
            ArgVal::IntArg(_) => match val_str.parse::<i32>(){
                    Ok(i) => ArgVal::IntArg(Some(i)),
                    _ => ArgVal::IntArg(None),
                },
            ArgVal::FloatArg(_) => match val_str.parse::<f32>(){
                    Ok(f) => ArgVal::FloatArg(Some(f)),
                    _ =>  ArgVal::FloatArg(None),
                },
        }
    }
}

fn parse_cmd_arguments(args: &'static str, arg_types: HashMap<&str, ArgVal>) -> HashMap<&'static str, ArgVal>
{
    let mut temp_cm = HashMap::new();

    let tokens_it = args.split_whitespace();
    for token in tokens_it
    {
        if token.starts_with('-')
        {
            let arg_name = &token[1..];
            let val_str = tokens_it.next();
            if let Some(val_str) = val_str
            {
                if let Some(arg_type) = arg_types.get(arg_name)
                {
                    temp_cm.insert(
                        arg_name,
                        ArgVal::new(val_str, arg_type)
                    );
                }
            }
        }
    }
    temp_cm
}*/
