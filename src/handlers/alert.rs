use super::super::update_handler::*;
use telegram_bot_fork::*;
use std::thread;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use futures;
use futures::Future;
use tokio;
use tokio_timer::Delay;
use meval::*;
use std::ops::Add;

pub struct AlertEl
{
    creation: Instant,
    message: String,
    answering: types::Message,
}

pub struct Alert
{
}

impl Alert
{
    pub fn new()-> Self
    {
        Self{}
    }

    pub fn alert_thread(api: &Api)
    {

    }
}

impl UpdateHandler for Alert
{
    fn get_cmd(&self) -> &str {"alert"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);
                {
                    match super::trim_command(self, data)
                    {
                        Some(trimmed) => {
                            let mut splitted = trimmed.split(' ');
                            if splitted.clone().count() < 2
                            {
                                api.spawn(message.text_reply("two arguments expected"));
                                return
                            }
                            let (time, message_text) = (splitted.next().unwrap(), splitted.collect::<Vec<&str>>().join(" ").to_owned()); 
                            let mut ctx = Context::new();
                            ctx.var("m", 60.0);
                            ctx.var("h", 3600.0);
                            match eval_str_with_context(time, ctx)
                            {
                                Ok(res) => {
                                    let time_delay = Delay::new(Instant::now().add(Duration::from_secs(res as u64))).from_err();
                                    let alert_api = api.clone();
                                    let answer_message = message.clone();
                                    let alert_future = api.send(message.text_reply("you will be reminded"))
                                        .join(time_delay)
                                        .map_err(|err| println!("error: {:?}", err))
                                        .and_then(move |(_, _)| {
                                            alert_api.spawn(answer_message.text_reply(message_text));
                                            Ok(())
                                        });
                                    tokio::executor::current_thread::spawn(alert_future);
                                }
                                Err(error) => api.spawn(message.text_reply("time parsing error")) 
                            }
                        }
                        None => {
                            api.spawn(message.text_reply("please provide arguments"))
                        }
                    }
                };
            }
        }
    }
}
