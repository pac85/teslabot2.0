use super::super::update_handler::*;
use telegram_bot_fork::*;
use meval::*;

pub struct Calc
{

}

struct Processed
{
    processed: String,
    bh: (bool, bool),
}

impl Calc
{
    pub fn new()-> Self
    {
        Self{}
    }

    fn preprocess(cmd: &str) -> Processed
    {
        let mut bin = false;
        let mut hex = false;

        let mut processed = String::new();

        for token in cmd.to_owned()
            .replace('+', " + ")
            .replace('-', " - ")
            .replace('*', " * ")
            .replace('/', " / ")
            .replace('^', " ^ ")
            .split(' ')
        {
            if token.starts_with("0x")
            {
                hex = true;
                processed += " ";
                match &i64::from_str_radix(&token[2..], 16)
                {
                    Ok(val) => {processed += &val.to_string();},
                    Err(_) => {processed += token;}
                };
            }
            else if token.starts_with("0b")
            {
                bin = true;
                processed += " ";
                match &i64::from_str_radix(&token[2..], 2)
                {
                    Ok(val) => {processed += &val.to_string();},
                    Err(_) => {processed += token;}
                };
            }
            else
            {
                processed += " ";
                processed += token;
            }
        }

        Processed {processed, bh: (bin, hex)}
    }

    fn format_output(result: &f64, processed: Processed) -> String
    {
        if !processed.bh.0 && !processed.bh.1
        {
            return format!("{}", result);
        }

        let mut formatted = format!("dec: {}", result);

        if processed.bh.0
        {
            formatted += &format!("\nbin: {:b}", result.round() as i64)
        }
        if processed.bh.1
        {
            formatted += &format!("\nhex: {:x}", result.round() as i64)
        }

        formatted
    }

    fn get_contex() -> Context<'static>
    {
        let mut ctx = Context::new();
        ctx.func("to_inches", |x| x*0.393701);
        ctx.func("from_inches", |x| x/0.393701);
        ctx.var("i", 1.0/0.393701);
        ctx.var("cm", 0.393701);
        ctx
    }
}

impl UpdateHandler for Calc
{
    fn get_cmd(&self) -> &'static str {"calc"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);

                let cmd_str = super::trim_command(self, &data).unwrap_or("0");
                let processed = Calc::preprocess(cmd_str);

                match eval_str_with_context(&processed.processed, Self::get_contex())
                {
                    Ok(result) =>{
                        api.spawn(message.text_reply(
                            format!("{}", Calc::format_output(&result, processed))
                        ));
                    }
                    Err(error) =>{
                        api.spawn(message.text_reply(
                            format!("sintax error: \n{}", error)
                        ));
                    }
                }
            }
        }
    }
}
