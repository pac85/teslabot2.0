use super::super::update_handler::*;
use telegram_bot_fork::*;
use structopt::StructOpt;
use std::f32::consts::PI;
use std::cmp::max;
use structopt::clap::AppSettings;

pub struct Inductor
{

}

impl Inductor
{
    pub fn new()-> Self
    {
        Self{}
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "example", about = "An example of StructOpt usage.")]
struct InductorArgs
{
    #[structopt(short = "n", long = "turns")]
    turns: u32,
    #[structopt(short = "d", long = "diameter")]
    diameter: f32,
    #[structopt(short = "l", long = "length")]
    length: f32,
    #[structopt(short = "u", long = "permeability", default_value = "1.2566370614e-6")]
    permeability: f32,
}

impl InductorArgs
{
    fn get_L(&self) -> f32
    {
        self.permeability*(self.turns*self.turns) as f32 *(PI*self.diameter*self.diameter/(4*self.turns) as f32)
    }

    fn get_L_str(&self) -> String
    {
        let divider = (1000.0/ self.get_L().max(0.0000001)) as u32;
        let divided = self.get_L() * (divider as f32);
        divided.to_string() + match divider{
            1 => "mH",
            1000 => "uH",
            1000000 => "nH",
            _ => "",
        }
    }
}

impl UpdateHandler for Inductor
{
    fn get_cmd(&self) -> &'static str {"inductor"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);

                let cmd_str = super::trim_command(self, &data).unwrap_or("");
                let mut cmd_ostr = String::new();
                cmd_ostr.push_str(cmd_str);
                let inductor = InductorArgs::from_iter_safe(cmd_ostr.split(" "));
                InductorArgs::clap().setting(AppSettings::ColorNever) ;
                match inductor
                {
                    Ok(inductor) => {
                        api.spawn(message.text_reply(
                            format!("{}", inductor.get_L_str())
                        ));
                    }
                    Err(error) => {
                        api.spawn(message.text_reply(
                            format!("{}", error)
                        ));
                    }
                }
            }
        }
    }
}
