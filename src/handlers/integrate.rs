use super::super::update_handler::*;
use telegram_bot_fork::*;
use meval::*;
use scan_fmt;

pub struct Integrate
{

}

impl Integrate
{
    pub fn new() -> Self
    {
        Self{}
    }

    pub fn integrate(a: f64, b: f64, expr: &str) -> String
    {
        "hello".to_owned()
    }
}

impl UpdateHandler for Integrate
{
    fn get_cmd(&self) -> &'static str {"integrate"}


    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);

                let cmd_str = super::trim_command(self, &data).unwrap_or("0");

                if let Ok((a, b, expr)) = scan_fmt!(cmd_str, "{f} {f} {}", f64, f64, String)
                {
                    api.spawn(message.text_reply(
                        Integrate::integrate(a, b, &expr)
                    ));
                }
                else
                {
                    api.spawn(message.text_reply(
                       "error, usage: a b *expression*".to_owned() 
                    ));
                }
            }
        }
    }
}
