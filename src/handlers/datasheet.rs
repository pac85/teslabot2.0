extern crate reqwest;
extern crate curl;
extern crate scraper;

use super::super::update_handler::*;
use telegram_bot_fork::*;
use futures::Future;
use self::scraper::{Html, Selector};
use self::curl::easy::Easy;
use std::cell::RefCell;

pub struct Datasheet
{

}

impl Datasheet
{
    pub fn new()-> Self
    {
        Self{}
    }

    fn scrape(page_code: String) -> Option<String>
    {
        let document = Html::parse_document(&page_code);
        let selector = Selector::parse("#cell10 > td:nth-child(3) > a:nth-child(1)").unwrap();
        Some(document.select(&selector).next()?.value().attr("href")?.to_string())
    }

    fn get_link(part_name: &str) -> Option<String>
    {
        /*let page_code = match reqwest::get(format!("http://www.alldatasheet.com/view.jsp?Searchword={}", part_name).as_str())
        {
            Ok(mut res) => res.text().unwrap_or("internal error".to_string()),
            Err(_) => "internal error".to_string(),
        };*/
        let page_code = RefCell::new("".to_owned());
        {
            let mut easy = Easy::new();
            easy.post(true);
            easy.url(format!("https://www.alldatasheet.com/view.jsp?Searchword={}", part_name).as_str()).unwrap();
            easy.http_headers({
                let mut list = curl::easy::List::new();
                list.append("user-agent: reqwest/0.9.11");
                list
            });
            let mut transf = easy.transfer();
            transf.write_function(|data| {
                page_code.borrow_mut().push_str(std::str::from_utf8(data).unwrap());
                Ok(data.len())
            }).unwrap();
            transf.perform().unwrap();
        }

        Self::scrape(page_code.into_inner())
    }

    fn get_pdf(part_name: &str) -> Option<String>
    {
        let mut link = Self::get_link(part_name)?;

        let viewlink = link.replace("//www.alldatasheet.com/datasheet-pdf/pdf", "https://pdf1.alldatasheet.com/datasheet-pdf/view");

        let page_code = RefCell::new("".to_owned());
        let headers = RefCell::new("".to_owned());
        {
            let mut easy = Easy::new();
            easy.post(true);
            easy.url(viewlink.as_str()).unwrap();
            easy.http_headers({
                let mut list = curl::easy::List::new();
                list.append("user-agent: reqwest/0.9.11");
                list.append(format!("Referer: {}", link).as_str());
                list
            });
            let mut transf = easy.transfer();
            transf.write_function(|data| {
                page_code.borrow_mut().push_str(std::string::String::from_utf8_lossy(data).to_mut());
                Ok(data.len())
            }).unwrap();
            transf.header_function(|data| {
                headers.borrow_mut().push_str(std::string::String::from_utf8_lossy(data).to_mut());
                true
            });
            transf.perform().unwrap();
        }

        let document = Html::parse_document(&page_code.into_inner());
        let selector = Selector::parse("div.overflowyo:nth-child(14) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > iframe:nth-child(1)").unwrap();
        let pdf_url = document.select(&selector).next()?.value().attr("src")?.as_bytes();

        let cookies_str = headers.borrow()
            .split('\n')
            .filter(|line| line.starts_with("Set-Cookie:"))
            .map(|line| line[12..].trim())
            .collect::<Vec<_>>()
            .join(";");

        Some("datasheetscrap.herokuapp.com?".to_owned() +
             "url=" + &Easy::new().url_encode(pdf_url)+"&"+
             "cookies=" + &Easy::new().url_encode(cookies_str.as_bytes()))

    }
}

impl UpdateHandler for Datasheet
{
    fn get_cmd(&self) -> &'static str {"datasheet"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        let mut part_name = None;
        let mut or_message = None;
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);
                part_name = super::trim_command(self, &data);
                or_message = Some(message);
                /*api.spawn(message.text_reply(
                    if let Some(peace_name) = super::trim_command(self, &data)
                    {
                        //Datasheet::get_link(peace_name)
                    }
                    else
                    {
                        format!("part name expected")
                    }
                ));*/
            }
        }
        if let Some(part_name) = part_name
        {
            let edit_api = api.clone();
            let part_name = part_name.to_owned();
            let fut = api.send(or_message.unwrap().text_reply("retreiving datasheet...")).and_then(move |loading_message|
            {
                let link = Datasheet::get_pdf(&part_name).unwrap_or("error".to_owned());
                let mut reply = loading_message.edit_text(format!("[{}.pdf]({})", part_name, link));
                reply.parse_mode(types::ParseMode::Markdown);
                edit_api.spawn(reply);
                Ok(())
            });
            tokio::executor::current_thread::spawn(fut.map_err(|_| ()).map(|_| ()))
        }
        else
        {
            api.spawn(or_message.unwrap().text_reply("part name expected"));
        }
    }
}
