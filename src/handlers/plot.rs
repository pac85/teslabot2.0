use super::super::update_handler::*;
use telegram_bot_fork::*;
use meval::*;
use scan_fmt;
use plotters::prelude::*;

use std::cmp::{min, max};
use std::convert::{TryInto, TryFrom};

pub struct Plot
{

}

impl Plot
{
    pub fn new()-> Self
    {
        Self{}
    }

    fn get_contex() -> Context<'static>
    {
        let mut ctx = Context::new();
        ctx.func("to_inches", |x| x*0.393701);
        ctx.func("from_inches", |x| x/0.393701);
        ctx.var("i", 1.0/0.393701);
        ctx.var("cm", 0.393701);
        ctx
    }

    fn get_plot(expr: String, beginning: Option<f64>, end: f64) -> Result<Vec<u8>, Box<dyn std::error::Error>>
    {
        const dimension: (u32, u32) = (640, 480);
        let mut context = Self::get_contex();
        let mut y_range = (std::f64::MAX, std::f64::MIN);
        let mut y_buffer: Vec<f64> = Vec::with_capacity((dimension.1+1).try_into().unwrap());

        for ix in 0..dimension.1
        {
            let x = (ix as f64/dimension.1 as f64) * end-beginning.unwrap_or(0f64)+beginning.unwrap_or(0f64);

            context.var("x", x);
            y_buffer[usize::try_from(ix).unwrap()] = match eval_str_with_context(&expr, &context)
            {
                Ok(result) => result,
                Err(error) => std::f64::MAX,
            };
            y_range = (y_range.0.min(y_buffer[usize::try_from(ix).unwrap()]), y_range.1.max(y_buffer[usize::try_from(ix).unwrap()]));
        }

        let mut buffer = Vec::with_capacity((dimension.0*dimension.1).try_into().unwrap());

        {
            let root = BitMapBackend::with_buffer(&mut buffer, dimension).into_drawing_area();
            root.fill(&White);

            let mut chart = ChartBuilder::on(&root)
                .caption(&expr, ("Arial", 35).into_font())
                .margin(5)
                .x_label_area_size(30)
                .y_label_area_size(30)
                .build_ranged(beginning.unwrap_or(0f64)..end, y_range.0..y_range.1)?;

            chart.configure_mesh().draw()?;

            chart.draw_series(LineSeries::new(
                (0..dimension.1)
                    .map(|x| (usize::try_from(x).unwrap(), (x as f64/dimension.1 as f64) * end-beginning.unwrap_or(0f64)+beginning.unwrap_or(0f64)))
                    .map(|(ix, x)| (x, y_buffer[ix])),
                &Red,
            ))?
                .label(&expr)
                .legend(|(x,y)| Path::new(vec![(x,y), (x + 20,y)], &Red));

            chart.configure_series_labels()
                .background_style(&White.mix(0.8))
                .border_style(&Black)
                .draw()?;
        }
        Ok(buffer)
    }

    fn parse_input(inp: String) -> Result<(String, Option<f64>, f64), Box<dyn std::error::Error>>
    {
        scan_fmt!(&inp, "{d} {d} | {}", f64, f64, String)
            .map(|(a, b, s)| (s, Some(a), b))
            .map_err(|e| Box::new(e) as Box<dyn std::error::Error>)
    }
}

impl UpdateHandler for Plot
{
    fn get_cmd(&self) -> &'static str {"plot"}

    fn on_update(&mut self, update: &Update, api: &Api)
    {
        if let UpdateKind::Message(ref message) = update.kind
        {
            if let MessageKind::Text {ref data, ..} = message.kind
            {
                println!("<{}>: {}", &message.from.first_name, data);

                let cmd_str = super::trim_command(self, &data).unwrap_or("0");

            }
        }
    }
}
