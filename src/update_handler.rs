use telegram_bot_fork::*;

pub trait UpdateHandler
{
    fn get_cmd(&self) -> & str;
    fn on_update(&mut self, update: &Update, api: &Api);
}

pub struct Handlers
{
    handlers: Vec<Box<UpdateHandler>>,
    api: Api,
}

impl Handlers
{
    pub fn new(api: Api) -> Self
    {
        Self{handlers: Vec::new(), api}
    }

    pub fn get_api(&self) -> Api
    {
        self.api.clone()
    }

    pub fn add_handler(&mut self, handler: Box<UpdateHandler>)
    {
        self.handlers.push(handler);
    }

    pub fn handle_update(&mut self, update: &Update, api: &Api)
    {
        for handler in self.handlers.iter_mut()
        {
            let mut to_match = String::new();
            to_match += handler.get_cmd();
            to_match += " ";

            match update.kind
            {
                UpdateKind::Message(Message{kind:MessageKind::Text{ref data,..},..},..)
                if data.len() >= handler.get_cmd().len() && data.trim_matches('/').to_lowercase().starts_with(&to_match[..to_match.len().min(data.len())])=> {
                    handler.on_update(&update, api);
                }
                _ => {
                    // println!("qualsiasi altro caso");
                }
            }
        }
    }

    pub fn get_handler(&self, command: &str) -> Option<&Box<UpdateHandler>>
    {
        for handler in self.handlers.iter()
        {
            if handler.get_cmd() == command
            {
                return Some(handler);
            }
        }
        None
    }
}
